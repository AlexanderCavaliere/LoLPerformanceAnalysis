;; Make RESTful calls to Riot Game's API
(ql:quickload :dexador)
(ql:quickload :yason)
(ql:quickload "split-sequence")
;; Globals
(defparameter *personal-api-key*   "RGAPI-6da132d5-c014-494a-b50f-52c5803ac5d9")
(defparameter *username*           "Kegmaker51")
(defparameter *riot-api-url*       "https://na1.api.riotgames.com/lol/")
(defparameter *riot-api-tail*      (concatenate 'string "?api_key=" *personal-api-key*))
(defparameter *summoner-id*        "23536018")
(defparameter *account-id*         "37647755")

;; Data endpoints
(defparameter *summoner-endpoint*       "summoner/v3/summoners/by-name/")
(defparameter *match-history-endpoint*  "match/v3/matchlists/by-account/")
(defparameter *match-endpoint*          "match/v3/matches/")
(defparameter *mastery-endpoint*        "champion-mastery/v3/champion-masteries/by-summoner/")

;; Keywords
(defparameter *summoner*      "summoner")
(defparameter *match-history* "match history")
(defparameter *match*         "match")
(defparameter *mastery*       "mastery")
(defparameter *game-id*       "gameId")

;; Functionality
(defun get-raw-data (data-type match-id)
  ;; query API based on type of data
  (let ((final-url ""))
    (cond ((string-equal data-type *summoner*)      (setf final-url (concatenate 'string *riot-api-url* *summoner-endpoint* *username* *riot-api-tail*)))
          ((string-equal data-type *match*)         (setf final-url (concatenate 'string *riot-api-url* *match-endpoint* match-id *riot-api-tail*)))
          ((string-equal data-type *mastery*)       (setf final-url (concatenate 'string *riot-api-url* *mastery-endpoint* *summoner-id* *riot-api-tail*)))
          ((string-equal data-type *match-history*) (setf final-url (concatenate 'string *riot-api-url* *match-history-endpoint* *account-id* *riot-api-tail*)))
          (t (print "NOTHING")))
    (dex:get final-url)))
;;(dex:get (concatenate 'string *riot-api-url* *summoner-endpoint* *username* *riot-api-tail*))

;; Take the JSON formatted string and turn it into a CLOS object
(defun marshal-raw-data (raw-data) (yason:parse raw-data))

;; Summoner JSON Format {profileIconId : int, name : string, summonerLevel : long, revisionDate : long, id : long, accountId : long}
(defun convert-summoner-json (summoner-json) (alexandria:hash-table-alist summoner-json))
;; Match List JSON Format { matches : List[Match], totalGames : int, startIndex : int, endIndex : int }
(defun convert-match-list-json (match-list-json) (alexandria:hash-table-alist match-list-json))
;; Match JSON Format { lane : string, gameId : long, champion : int, platformId : string, season : int, queue : int, role : string, timestamp : long }
(defun convert-match-json (match-json) (alexandria:hash-table-alist match-json))
;; Match by Match ID JSON Format { seasonId : int, queueId : int, gameId : long, participantIdentities : List[ParticipantID], gameVersion : string, platformId : string, gameMode : string, mapId : int, gameType : string, teams : List[TeamStats], participants : List[Participants], gameDuration : long, gameCreation : long }
(defun convert-match-by-id-json (match-json) (alexandria:hash-table-alist match-json))
;; Extract ParticipantID data { player : Player, participantId : long }
(defun convert-participant-id-json (participant-json) (alexandria:hash-table-alist participant-json))
;; Player data {currentPlatformId : string, summonerName : string, matchHistoryUri : string, platformId : string, currentAccountid : long, profileIcon : int, summonerId : long, accountId : long }
(defun convert-player-json (player-json) (alexandria:hash-table-alist player-json))
;; Participant data { stats : ParticipantStats, participantId : int, runes : Runesobject, timeline : ParticipantTimeline, teamId : int, spell2Id: int, masteries : List[MasteryObject], higestAchievedSeasonTier : stirng, spell1d : int, championId : int }
(defun convert-participant-json (participant-json) (alexandria:hash-table-alist participant-json))
;; Participant Stats {physicalDamagedealt : long, neutralMinioinsKilledTeamJunle : int, magicDamageDealt : long, totalPlayerScore : int, deaths : int, win : boolean, neutralMinionsKilledEnemyJungle : int, altarsCapture : int, largestCriticalStrike : int, totalDamageDealt : long, magicDamageDealtToChampions : long, visionWardsBoughtInGame : int, damageDealtToObjectives : long, largestKillingSpree : int, item1 : int, quadraKills : int, teamObjective : int, totalTimeCrowdControlDealt : int, longestTimeSpentLiving : int, wardsKilled : int, firstTowerAssist : boolean, firstTowerKill : boolean, item2: int, item3 : int, item0 : int, firstBloodAssist : boolean, visionScore : long, wardsPlaced : int, item4 : int, item5 : int, item6 : int, turretKills : int, tripleKills : int, damageSelfMitigated : long, champLevel : int, nodeNeutralizeAssist : int, firstInhibitorKill : boolean, goldEarned : int, magicalDamageTaken : long, kills : int, doubleKills : int, nodeCaptureAssist : int, trueDamagetaken : long, nodeNeutralized : int, firstInhibitorAssist : boolean, assists : int, unrealKills : int, neutralMinionsKilled : int, objectivePlayerscore : int, combatplayerScore : int, damageDealtToTurrets : long, altarsNeutralized : int, physicalDamageDealtToChampions : long, goldSpent : int, trueDamageDealt : long, trueDamageDealtToChampions : long, participantId : int, pentaKills : int, totalHeal : long, totalMinionsKilled : int, firstBloodKill : boolean, nodeCapture : int, largestMultiKill : int, sightWardsBoughtInGame : int, totalDamageDealtToChampions : long, totalUnitsHealed : int, inhibitorKills : int, totalScoreRank : int, totalDamageTaken : long, killingSprees : int, timeCCingOthers : long, physicalDamageTaken : long}
(defun convert-participant-stats-json (participant-json) (alexandria:hash-table-alist participant-json))
;; Return an assoc list of a summoner's matches
(defun get-match-list () (mapcar #'convert-match-json (cdr (car (cdddr (convert-match-list-json (marshal-raw-data (get-raw-data *match-history* nil))))))))

;; Return the id of a game
(defun get-match-id (alist-match) (cdr (assoc *game-id* alist-match :test #'string-equal)))

;; Returns a list of match ids
(defun display-match-ids (match-list)
  (if (eq (car match-list) nil)
      nil
      (cons (get-match-id(car match-list)) (display-match-ids(cdr match-list)))))

;; Prints matches from oldest to newest
(defun print-match-ids (match-list index)
  (if (eq match-list nil) nil
      (progn
        (print-match-ids (cdr match-list) (+ index 1))
        (print (concatenate 'string "[" (write-to-string index :base 10) "] " (write-to-string (car match-list) :base 10))))))

;; Choose a match by ID
(defun select-match-by-id (match-list selection position)
  (if (or (eq match-list nil) (eq selection position) )
      (car match-list)
      (select-match-by-id (cdr match-list) selection (+ 1 position))))

;; Retreive data associated with a specific match
(defun get-match (match-id) (convert-match-by-id-json (marshal-raw-data (get-raw-data *match* (write-to-string match-id :base 10)))))

;; ParticipantID Data format ( 10x Hash Tables)
;; Grab stats associated with player
;; Take match data, find summoner name, use that to get participant number, extract stats
(defun extract-player-data (participant-data)
  (labels ((player-list (players)
             (if (eq players nil)
                 nil
                 (cons (alexandria:hash-table-alist (car players))
                       (player-list (cdr players)))))
           (get-player-hashlist (players) ;;extract nested hash-lists
             (cdr (assoc "player" players :test #'string-equal )))
           (convert-player-hash-table (player-hash-tables)
             (alexandria:hash-table-alist player-hash-tables)) ;; turn hash-tables into assoc. lists
           (grab-name (identities)
             (cdr (assoc "summonerName" identities :test #'string-equal)))
           (build-list (players) ;; get all the participant data into one list
             (mapcar #'convert-player-hash-table (mapcar #'get-player-hashlist (player-list (cdar players)))))
           (build-summoner-list (summoner-info) ;; extract summoner names from list
             (mapcar #'grab-name summoner-info))
           (get-participant-id (summoner-names position name) ;; Build a list of summoner names
             (if (string-equal (car summoner-names) name)
                 position
                 (get-participant-id (cdr summoner-names) (+ 1 position) name)))
           (get-stat-list (data) ;; Pull out hash tables from stats list
             (cdr (cadr data)))
           (extract-player-stats (stats position participant-id) ;; find stats of target summoner
             (if (eq position participant-id)
                 (car stats)
                 (extract-player-stats (cdr stats) (+ 1 position) participant-id))))
    ;; Body
    (convert-player-hash-table (extract-player-stats (get-stat-list participant-data) 1
    (get-participant-id
     (build-summoner-list
      (build-list participant-data)) 1 *username*)))))

(defun extract-from-match-data (match-data data-key-string)
  (cdr (assoc data-key-string match-data :test #'string-equal)))

(defun build-match-alist (data)
  (if (eq data nil)
      nil
      (cons (alexandria:hash-table-alist (car data)) (build-match-alist (cdr data)))))

(defun analyze-role (data role)
  (mapcar #'get-match data))
;; Data Extraction
(defun access-player-data (player-data key)
  (cdr (assoc key player-data :test #'string-equal)))


