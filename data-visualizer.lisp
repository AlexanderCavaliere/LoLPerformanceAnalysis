;; Spit out a graph of data
;; Load in the plotting library
(ql:quickload :vgplot)


(defun create-graph (list-x list-y graph-type)
  ;; Creates a 2D graph of the specified type
  (if (string-equal "bar" graph-type)
      (vgplot:bar 'list-x 'list-y)  ;; true
      (vgplot:plot list-x list-y)))

